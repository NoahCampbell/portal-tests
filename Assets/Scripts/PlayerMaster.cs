using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMaster : MonoBehaviour
{
    [Header("Movement")]
    [System.NonSerialized] public float speed = 2f;
    public bool isGrounded;
    [System.NonSerialized] public float gravity = -9.81f;
}
