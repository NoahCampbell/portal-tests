using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalCreator : MonoBehaviour
{
    public GameObject orangePortalPrefab;
    public GameObject bluePortalPrefab;
    GameObject orangePortal;
    GameObject bluePortal;
    public GameObject player;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.anyKey)
        {
            CheckInput();
        }
    }

    void CheckInput()
    {
        if (Input.GetButtonDown("Portal1"))
        {
            if (orangePortal != null)
            {
                Destroy(orangePortal);
            }

            CreateOrangePortal();
        }
        if (Input.GetButtonDown("Portal2"))
        {
            if (bluePortal != null)
            {
                Destroy(bluePortal);
            }

            CreateBluePortal();
        }

    }

    void CreateOrangePortal()
    {
        RaycastHit hit;
        bool isHit = Physics.Raycast(player.transform.position, player.transform.forward, out hit, 50f);
        Quaternion finalRotation = Quaternion.identity;

        if (isHit)
        {
            orangePortal = Instantiate(orangePortalPrefab);
            orangePortal.transform.position = hit.point;
            

            
            if (hit.transform.gameObject.tag == "Ground")
            {
                PositionCorrection("Wall", orangePortal);
                finalRotation.eulerAngles = new Vector3(90f, player.transform.rotation.eulerAngles.y, 0f);
                orangePortal.transform.rotation = finalRotation;
            }
            else if (hit.transform.gameObject.tag.Contains("Portal"))
            {
                orangePortal.transform.rotation = hit.transform.rotation;
                orangePortal.transform.position = hit.transform.position;
            }
            else if (hit.transform.gameObject.tag == "Wall")
            {
                PositionCorrection("Ground", orangePortal);

                var forwardDir = orangePortal.transform.TransformDirection(orangePortal.transform.forward);
                var toPlayer = player.transform.position - orangePortal.transform.position;

                float dotProd = Vector3.Dot(forwardDir, toPlayer);
                
                if (dotProd > 0)
                {
                    orangePortal.transform.Rotate(0f, 180f, 0f);
                }
            }


            if (bluePortal != null)
            {
                orangePortal.GetComponent<PortalMaster>().connectedPortal = bluePortal;
                bluePortal.GetComponent<PortalMaster>().connectedPortal = orangePortal;
            }
        }
    }

    void CreateBluePortal()
    {
        RaycastHit hit;
        bool isHit = Physics.Raycast(player.transform.position, player.transform.forward, out hit, 50f);
        Quaternion finalRotation = Quaternion.identity;

        if (isHit)
        {
            bluePortal = Instantiate(bluePortalPrefab);
            bluePortal.transform.position = hit.point;

            if (hit.transform.gameObject.tag == "Ground")
            {
                bluePortal.transform.rotation = Quaternion.LookRotation(Vector3.up, Vector3.up);
                PositionCorrection("Wall", bluePortal);

                finalRotation.eulerAngles = new Vector3(90f, player.transform.eulerAngles.y, 0f);

                bluePortal.transform.rotation = finalRotation;
            }
            else if (hit.transform.gameObject.tag.Contains("Portal"))
            {
                bluePortal.transform.rotation = hit.transform.rotation;
                bluePortal.transform.position = hit.transform.position;
            }
            else if (hit.transform.gameObject.tag == "Wall")
            {
                PositionCorrection("Ground", bluePortal);

                var forwardDir = bluePortal.transform.TransformDirection(bluePortal.transform.forward);
                var toPlayer = player.transform.position - bluePortal.transform.position;

                float dotProd = Vector3.Dot(forwardDir, toPlayer);
                
                if (dotProd > 0)
                {
                    bluePortal.transform.Rotate(0f, 180f, 0f);
                }
            }
            

            if (orangePortal != null)
            {
                bluePortal.GetComponent<PortalMaster>().connectedPortal = orangePortal;
                orangePortal.GetComponent<PortalMaster>().connectedPortal = bluePortal;
            }
        }
    }

    void PositionCorrection(string tag, GameObject portal)
    {
        var topCheck = portal.transform.Find("PortalTop");
        var bottomCheck = portal.transform.Find("PortalBottom");
        var leftCheck = portal.transform.Find("LeftCheck");
        var rightCheck = portal.transform.Find("RightCheck");

        Vector3 difference = Vector3.zero;

        RaycastHit hit;
        bool isHit = Physics.Raycast(bottomCheck.transform.position, bottomCheck.transform.up, out hit, 6f);

        if (isHit)
        {
            if (hit.transform.gameObject.tag == tag)
            {
                difference = hit.point - bottomCheck.transform.position;
                portal.transform.position += difference;
            }
        }
        else
        {
            isHit = Physics.Raycast(topCheck.transform.position, -topCheck.transform.up, out hit, 6f);

            if (isHit)
            {
                if (hit.transform.gameObject.tag == tag)
                {
                    difference = hit.point - topCheck.transform.position;
                    portal.transform.position -= difference;
                }
            }
        }

        isHit = Physics.Raycast(leftCheck.transform.position, leftCheck.transform.right, out hit, 3f);

        if (isHit)
        {
            if (hit.transform.gameObject.tag == tag)
            {
                difference = hit.point - leftCheck.transform.position;
                portal.transform.position += difference;
            }
        }
        else
        {
            isHit = Physics.Raycast(rightCheck.transform.position, -rightCheck.transform.right, out hit, 3f);

            if (isHit)
            {
                difference = hit.point - rightCheck.transform.position;
                portal.transform.position -= difference;
            }
        }
    }
}
