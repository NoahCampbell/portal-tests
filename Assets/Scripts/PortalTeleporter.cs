using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalTeleporter : MonoBehaviour
{
    PortalMaster portalMaster;
    bool movingBackwards;
    bool movingForwards;

    // Start is called before the first frame update
    void Start()
    {
        portalMaster = GetComponent<PortalMaster>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" && portalMaster.connectedPortal != null && !portalMaster.justTeleported)
        {
            var forwardDir = other.transform.TransformDirection(Vector3.forward);
            var toPortal = transform.position - other.transform.position;

            float dotProd = Vector3.Dot(forwardDir, toPortal);

            if (dotProd < -0.4f)
            {
                movingBackwards = true;
            }
            else if (dotProd > 0.4f)
            {
                movingForwards = true;
            }

            GameObject player = other.gameObject;
            player.transform.position = portalMaster.connectedPortal.transform.Find("PlayerTeleport").transform.position;
            SetPlayerRotation(player);            

            if (transform.gameObject.tag == "Blue Portal")
            {
                portalMaster.insideBluePortal = true;
            }
            else if (transform.gameObject.tag == "Orange Portal")
            {
                portalMaster.insideOrangePortal = true;
            }

            portalMaster.justTeleported = true;
            portalMaster.insidePortal = true;
            StartCoroutine(TeleportCooldown());
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player" && (!portalMaster.insideBluePortal && !portalMaster.insideOrangePortal))
        {
            portalMaster.insidePortal = false;
        }
        else
        {
            if (transform.gameObject.tag == "Blue Portal")
            {
                portalMaster.insideBluePortal = false;
            }
            else if (transform.gameObject.tag == "Orange Portal")
            {
                portalMaster.insideOrangePortal = false;
            }
        }
    }

    IEnumerator TeleportCooldown()
    {
        while (portalMaster.justTeleported)
        {
            if (!portalMaster.insidePortal)
            {
                portalMaster.justTeleported = false;
            }
            yield return new WaitForSeconds(0.001f);   
        }
    }

    void SetPlayerRotation(GameObject player)
    {
        var eulerRotation = player.transform.rotation.eulerAngles;

        if (movingForwards || movingBackwards)
        {
            eulerRotation = new Vector3(eulerRotation.x, eulerRotation.y + 180, eulerRotation.z);
            movingBackwards = false;
            movingForwards = false;
        }

        eulerRotation.x = portalMaster.connectedPortal.transform.rotation.eulerAngles.x;
        eulerRotation.z = portalMaster.connectedPortal.transform.rotation.eulerAngles.z;

        var finalRotation = Quaternion.identity;
        finalRotation.eulerAngles = eulerRotation;

        player.transform.rotation = finalRotation;
        player.GetComponent<Collider>().transform.rotation = player.transform.rotation;
    }
}
