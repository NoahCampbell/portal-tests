using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    Vector3 move;
    float horizontalRotation;
    float verticalRotation;
    Rigidbody rb;
    PlayerMaster player;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        player = GetComponent<PlayerMaster>();
    }

    // Update is called once per frame
    void LateUpdate()
    {
        Move();
        Rotate();
        ApplyGravity();
    }

    void Move()
    {
        move = transform.right * Input.GetAxis("Horizontal") + transform.forward * Input.GetAxis("Vertical");

        move = move.normalized * player.speed * Time.deltaTime;

        transform.Translate(move, Space.World);
    }

    void Rotate()
    {
        horizontalRotation = Input.GetAxis("Mouse X");
        verticalRotation = Input.GetAxis("Mouse Y");


        transform.Rotate(0, horizontalRotation, 0);
        Camera.main.transform.Rotate(-verticalRotation, 0, 0);
    }

    void ApplyGravity()
    {
        RaycastHit hit;
        bool isHit = Physics.Raycast(transform.position, -transform.up, out hit, 1.1f);

        if (isHit)
        {
            if (hit.transform.gameObject.tag == "Ground" || hit.transform.gameObject.tag == "Wall")
            {
                player.isGrounded = true;
            }
            else
            {
                player.isGrounded = false;
            }
        }
        else
        {
            player.isGrounded = false;
        }
            
        if (!player.isGrounded)
        {
            var gravityForce = transform.up * player.gravity * Time.deltaTime;
            
            transform.Translate(gravityForce);
        }
    }
}
