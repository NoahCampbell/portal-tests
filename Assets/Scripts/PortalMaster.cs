using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalMaster : MonoBehaviour
{
    [Header("Portals")]
    public GameObject connectedPortal;
    public bool justTeleported = false;
    public bool insidePortal = false;
    public bool insideBluePortal;
    public bool insideOrangePortal;
}
